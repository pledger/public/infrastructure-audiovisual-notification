FROM alpine:3.12.1

RUN apk update &&\
    apk upgrade &&\
    apk add bash &&\
    apk add bash-doc &&\
    apk add bash-completion
RUN apk add gcc=9.3.0-r2
RUN apk add cmake=3.17.2-r0
RUN apk add make=4.3-r0
RUN apk add boost=1.72.0-r6
RUN apk add boost-dev=1.72.0-r6
RUN apk add git
RUN apk add openssl-dev
RUN apk add doxygen=1.8.18-r0
RUN apk add graphviz
RUN apk add cppunit-dev=1.15.1-r0
RUN apk add build-base=0.5-r2
RUN apk add gpsd=3.18.1-r2 --repository=http://dl-cdn.alpinelinux.org/alpine/v3.10/main
RUN apk add gpsd-dev=3.18.1-r2 --repository=http://dl-cdn.alpinelinux.org/alpine/v3.10/main
RUN apk add crypto++ --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
RUN apk add crypto++-dev --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
RUN git clone https://git.code.sf.net/p/geographiclib/code geographiclib-code && \
    cd geographiclib-code &&\
    mkdir BUILD &&\
    cd BUILD &&\
    cmake .. &&\
    make &&\
    make install &&\
    cd .. &&\
    cd .. &&\
    rm -r geographiclib-code
RUN git clone https://github.com/riebl/vanetza.git &&\
    cd vanetza/ &&\
    mkdir build/ &&\
    cd build/ &&\
    cmake .. &&\
    make &&\
    make install &&\
    cd .. &&\
    cd .. &&\
    rm -r -f vanetza/
ADD src/ /src
RUN cd src/ &&\
    mkdir build &&\
    cd build/ &&\
    cmake -D SOCKTAP_WITH_GPSD=ON .. &&\
    make &&\
    cp bin/obu /obu &&\
    cd .. &&\
    cd .. &&\
    rm -r -f src/
ENTRYPOINT [ "/obu" ]
CMD ["-p",  "static", "-i",  "lo"]