cmake_minimum_required(VERSION 3.12)
project(OBU-ITS VERSION 0.1)

set(CMAKE_CXX_STANDARD 11)

# project variables
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
set(CMAKECONFIG_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/obu-its")

# Look up threading library (usually pthread)
set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package(Threads MODULE)

# Look up GPS library (libgps)
find_package(GPS REQUIRED)

# Look up vanetza library
find_package(Vanetza REQUIRED)

# Mandatory Boost components
set(Boost_COMPONENTS date_time system program_options log)
find_package(Boost 1.58 REQUIRED COMPONENTS ${Boost_COMPONENTS})


if(NOT TARGET Boost::system)
    message(STATUS "Skip build of socktap because of missing Boost::system dependency")
    return()
endif()

if(NOT TARGET Boost::program_options)
    message(STATUS "Skip build of socktap because of missing Boost::program_options dependency")
    return()
endif()

find_package(Threads REQUIRED)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)


add_executable(obu
    application.cpp
    benchmark_application.cpp
    cam_application.cpp
    denm_application.cpp
    ldm.cpp
    dcc_passthrough.cpp
    ethernet_device.cpp
    hello_application.cpp
    link_layer.cpp
    main.cpp
    positioning.cpp
    raw_socket_link.cpp
    router_context.cpp
    security.cpp
    time_trigger.cpp
    udp_link.cpp
)

target_link_libraries(obu PUBLIC Boost::system Boost::program_options Boost::log Threads::Threads Vanetza::vanetza)
install(TARGETS obu EXPORT ${PROJECT_NAME} DESTINATION ${CMAKE_INSTALL_BINDIR})

option(SOCKTAP_WITH_COHDA_LLC "Use Cohda LLC API for socktap" ${COHDA_FOUND})
if(SOCKTAP_WITH_COHDA_LLC)
    find_package(Cohda MODULE REQUIRED)
    target_compile_definitions(obu PUBLIC "SOCKTAP_WITH_COHDA_LLC")
    target_include_directories(obu PUBLIC ${COHDA_INCLUDE_DIRS})
    target_sources(obu PRIVATE cohda.cpp cohda_link.cpp)
endif()

find_package(GPS QUIET)
option(SOCKTAP_WITH_GPSD "Enable gpsd positioning for socktap" ${GPS_FOUND})
if(SOCKTAP_WITH_GPSD)
    find_package(GPS REQUIRED)
    target_compile_definitions(obu PUBLIC "SOCKTAP_WITH_GPSD")
    target_link_libraries(obu PUBLIC GPS::GPS)
    target_sources(obu PRIVATE gps_position_provider.cpp)
endif()
