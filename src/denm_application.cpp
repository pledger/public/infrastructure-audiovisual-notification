#include "denm_application.hpp"
#include <vanetza/asn1/denm.hpp>
#include <vanetza/btp/ports.hpp>
#include <vanetza/asn1/packet_visitor.hpp>


DenmApplication::DenmApplication(vanetza::Runtime& rt): runtime_(rt), warning_(false), critical_(false){}

DenmApplication::PortType DenmApplication::port()
{
    return vanetza::btp::ports::DENM;
}

void DenmApplication::indicate(const DataIndication&, UpPacketPtr packet)
{
    std::cout << "Received DENM" << std::endl;
    vanetza::asn1::PacketVisitor<vanetza::asn1::Denm> visitor;
    std::shared_ptr<const vanetza::asn1::Denm> denm = boost::apply_visitor(visitor, *packet);
    switch((*denm)->denm.situation->eventType.subCauseCode){
        case 1:
            trigger_critical();
            break;
        case 2:
            trigger_warning();
            break;
    }

}

void DenmApplication::trigger_critical()
{
    if (critical_ || warning_)
    {
        runtime_.cancel(this);
    }
    critical_ = true;
    warning_ = false;
    std::cout << "Starting critical" << std::endl;
    runtime_.schedule(std::chrono::seconds(2), std::bind(&DenmApplication::cancel_critical, this), this);
}



void DenmApplication::trigger_warning()
{
    if (!critical_ && warning_){
        runtime_.cancel(this);
    }
    if(!critical_)
    {
        std::cout << "Starting Warning" << std::endl;
        warning_ = true;
        runtime_.schedule(std::chrono::seconds(2), std::bind(&DenmApplication::cancel_warning, this), this);
    }
}

void DenmApplication::cancel_critical()
{
    std::cout << "Stopped critical" << std::endl;
    critical_ = false;
}
void DenmApplication::cancel_warning()
{
    std::cout << "Stopped Warning" << std::endl;
    warning_ = false;
}