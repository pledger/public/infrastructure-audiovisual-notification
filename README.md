# Infrastructure Audiovisual Notification

The Infrastructure Audiovisual Notification module runs on the OBU side of the application. Which means that is run by every node involved within the vehicular-communications environment. In a nutshel, it's a module that runs the Cooperative Awareness Service of the ETSI C-ITS protocol stack. And gets the danger notifications coming from the Risk Detection Notification System to alert the vehicle user.

It uses a modified version of the open source VANETs project implementing the European standard, the vanetza; to implement all of the logic. 

## Architecture

The architecture of the present module is fairly simple. It's just the "OBU-app" module of the architecture layout:

![PLEDGER Notification system architecure](docs/pledger_architecture.png)

## Configuring

The configuration is fairly simple:

- **interface, i**: That's the interface to access the lower layers (IEEE 802.11p in the case of the PLEDGER project)
- **station_id**: The station ID used when sending vehicular messages. It must be unique within the vehicular environment. By default gets a random value. Which makes it really difficult to collide with other vehicles; but for tracking purposes it's recommended to fill in the numbers.
- **positioning**: By default static, to hook the module to a GPSD in order to function properly it **must** be set to **gpsd** and then give the address to the GPSD.
- **gpsd-host** Address of the GPSD
- **gpsd-port** Port of the GPSD. By default: 2947

Although the project could be run on-premise directly without any containerization; the truth is that, for simplicity purposes, it has been designed to be run that way; which means that the connection to the lower-layers interface must be properly configured.


## Building and running
The current module has been designed to be run as a docker container. To build the container there is enough with building the docker image specified in the *Dockerfile* file:
```
docker build -t infrastructure .
```
Once the docker image has been built, the module can be launched with simply running the container (with the appropriate configuration). As an example:
```
docker run infrastructure --positioning gpsd --gpsd-host localhost 
```

The module can be run withing a *docker-compose* configuration to automatically set up both the GPSD and **Infrastructure Audiovisual Communication**.


| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received funding by the European Commission under grant agreement No. 871536, Pledger project. |
|---|--------------------------------------------------------------------------------------------------------|