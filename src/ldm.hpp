#ifndef LDM_HPP
#define LDM_HPP

#include <vanetza/asn1/its/CoopAwareness.h>
#include <vanetza/common/runtime.hpp>
#include <list>
#include <map>

class LDM
{
public:
    LDM(vanetza::Runtime &rt);
    void new_position(const int &station_id, CoopAwareness_t position);
    // CoopAwareness_t last_position(const int &station_id);
    void print_known_positions();

private:
    std::map<int, std::list<CoopAwareness_t>> positions_;
    vanetza::Runtime &runtime_;
    void remove_old();
};

#endif