#include "ldm.hpp"
#include <iostream>
#include <vanetza/asn1/its/CoopAwareness.h>
#include <list>
#include <map>
#include <chrono>

LDM::LDM(vanetza::Runtime &rt) : runtime_(rt) {}

void LDM::new_position(const int &station_id, CoopAwareness_t position)
{
    if (positions_.find(station_id) == positions_.end())
    {
        positions_[station_id] = std::list<CoopAwareness_t>();
    }
    positions_[station_id].push_front(position);
}

// CoopAwareness_t LDM::last_position(const int &station_id)
// {
//     if (positions_.find(station_id) != positions_.end())
//     {
//         return positions_[station_id].front();
//     }
//     return nullptr;
// }

void LDM::remove_old()
{
    runtime_.schedule(std::chrono::seconds(30), std::bind(&LDM::remove_old, this), this);
    const auto time_now = std::chrono::duration_cast<std::chrono::milliseconds>(runtime_.now().time_since_epoch());
    uint16_t gen_delta_time = time_now.count();
    for (auto &x : positions_)
    {
        for (auto it = x.second.begin(); it!=x.second.end();)
        {
            if ((*it).generationDeltaTime < gen_delta_time - 120)
            {
                std::cout << "Removing old position!" << std::endl;
                x.second.erase(it);
            }
            ++it;
        }
    }
}

void LDM::print_known_positions()
{
    std::cout << "***** POSITIONS ******" << std::endl;
    for (auto const &x : positions_)
    {
        CoopAwareness_t pos = x.second.front();
        int station_id = x.first;
        float lat = pos.camParameters.basicContainer.referencePosition.latitude / 10000000;
        float lon = pos.camParameters.basicContainer.referencePosition.longitude / 10000000;
        std::cout << "Position of: " << station_id << std::endl;
        std::cout << "Latitude: " << lat << std::endl;
        std::cout << "Longitude: " << lon << std::endl;
    }
}